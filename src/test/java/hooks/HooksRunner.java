package hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class HooksRunner extends SeMethods {
	
	@Before
	public void beforeScenario(Scenario sc)
	{
		
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test=startTestCase(sc.getId());
		test.assignCategory("Smoke");
		test.assignAuthor("Prasanna");
		startApp("Chrome", "http://leaftaps.com/opentaps/");
		
	}
	@After
	public void afterScenario()
	{
		closeAllBrowsers();
		endResult();
	
	}
	

}
