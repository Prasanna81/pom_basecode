//package Steps;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class Steps {
//	public ChromeDriver driver;
//	@Given("Launch the Browser")
//	public void launchBrowser()
//	{
//		driver = new ChromeDriver();
//	}
//	@And("Load the URL")
//	public void loadURL()
//	{
//		driver.get("http://leaftaps.com/opentaps/");
//	}
//	@And("Maximize")
//	public void maximise()
//	{
//		driver.manage().window().maximize();
//	}
//	@And("TimeOut")
//	public void Timeout() throws InterruptedException
//	{
//		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
//		//driver.manage().timeouts().implicitlyWait(3000, Seconds)
//	}
//	@When("Enter the Username")
//	public void enterUSerName()
//	{
//		driver.findElementById("username").sendKeys("DemoSalesManager");
//	}
//	@And("Enter the Password")
//	public void enterPassword()
//	{
//		driver.findElementById("password").sendKeys("crmsfa");
//	}
//	@When("Click the Login button")
//	public void clickLogin()
//	{
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//	@Then("Login Successfull")
//	public void loginSuccess()
//	{
//		System.out.println("login success");
//	}
//
//	@When("Click the CRMFSA")
//	public void clickCrmFsa()
//	{
//		driver.findElementByLinkText("CRM/SFA").click();
//	}
//	@And("Click the Lead")
//	public void clickLead()
//	{
//		driver.findElementByLinkText("Leads").click();
//	}
//	@And("Click the CreateLead")
//	public void clickCreate1lead()
//	{
//		driver.findElementByLinkText("Create Lead").click();
//	}
//	@Then("CreateLead is clicked successfully")
//	public void createleadclicked()
//	{
//		System.out.println("CreateLead is clicked successfully");
//	}
//
//
//
//	@When("Enter the firstName as (.*)")
//	public void clickDname(String data)
//	{
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);
//	}
//
//
//	@And("Enter the LastName as (.*)")
//	public void clickEname(String data)
//	{
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);
//	}
//	@And("Enter the companyName as (.*)")
//	public void clickFName(String data)
//	{
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);
//	}
//
//	@And("Select the Industry")
//	public void selectIndustry()
//	{
//		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
//
//		Select st=new Select(industry);
//		st.selectByIndex(1);
//	}
//	@And("Select the currencyCode")
//	public void selectCurrency()
//	{
//		WebElement industry = driver.findElementById("Preferred Currency");
//
//		Select se=new Select(industry);
//		se.selectByIndex(2);
//		System.out.println("currency");
//	}
//	@And("Lead is Created")
//	public void leadcreated() 
//	{
//		driver.findElementByClassName("smallSubmit").click();
//		System.out.println("Clicked");
//	}
//
//	@Then("Lead is created successfully")
//	public void createdLead()
//	{
//		System.out.println("CreateLead  has been submitted successfully");
//	}
//
//}
