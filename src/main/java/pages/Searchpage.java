package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Searchpage extends ProjectMethods {

	public Searchpage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public Searchpage findLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLead);
		return this;
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement eleCreateClickLead;
	public CreateLeadPage clickCreateLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateClickLead);
		return new CreateLeadPage();
	}

		
		
		}
	






