package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	private WebElement eleUsername;
	@When("Enter the Username1")
	
	public LoginPage typeUsername() {
		//WebElement eleUsername = locateElement("id", "username");
		//type(eleUsername, data);
		//LoginPage lp = new LoginPage();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		return this;
	}
	@FindBy(id="password")
	private WebElement elePassword;
	@And("Enter the Password1")
	public LoginPage typePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
		//WebElement elePassword = locateElement("id", "password");
		//type(elePassword, data);
		return this;
		
	}
	@FindBy(className ="decorativeSubmit")
	private WebElement eleLogin;
	@When("Click the Login button1")
	public HomePage clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		//click(eleLogin);
		return new HomePage();
	}
}





