package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_firstName")
	private WebElement eleEnterFirstName;
	@When("Enter the firstName as (.*)")
	public  CreateLeadPage enterName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(eleEnterFirstName, data);
		return this;
	}
	@FindBy(id="createLeadForm_lastName")
	private WebElement eleEnterLastName;
	@And("Enter the LastName as (.*)")
	public CreateLeadPage enterlastName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(eleEnterLastName, data);
		return this;
	}
	@FindBy(id="createLeadForm_companyName")
	private WebElement eleCompNAme;
	@And("Enter the companyName as (.*)")
	public CreateLeadPage enterCompany(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(eleCompNAme, data);
		return this;
	}
	@FindBy(id="createLeadForm_firstNameLocal")
	private WebElement elelocalName;
	public CreateLeadPage eleLocalName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(elelocalName, data);
		return this;
	}
	
	
	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement ele;
	public CreateLeadPage marketingCampaig(int index) {
		selectDropDownUsingIndex(ele, index);
		return this;
		
		
		// TODO Auto-generated method stub
		
	}
	

	@FindBy(id="createLeadForm_industryEnumId")
	private WebElement eleindustry;
	@And("Select the Industry")
	public CreateLeadPage industry(int index) {
		selectDropDownUsingIndex(eleindustry, index);
		return this;
	}

		@FindBy(name="Preferred Currency")
		private WebElement eleCurrency;
		public CreateLeadPage currency(int index) {
			selectDropDownUsingIndex(eleCurrency, index);
			return this;
		}
		
		//Preferred Currency
		
		// TODO Auto-generated method stub
		
	
		
		@FindBy(className="smallSubmit")
		private WebElement eleClickCreateLead;
		@And("Lead is Created")
		public ViewPage clickCreateLead() {
			
			//WebElemeneleClickCreateLeadagaint eleLogout = locateElement("class", "decorativeSubmit");
			click(eleClickCreateLead);
			return new ViewPage();
		}
	}
	





