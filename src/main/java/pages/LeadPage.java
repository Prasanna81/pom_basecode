package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class LeadPage extends ProjectMethods {

	public LeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Leads")
	private WebElement eleClickLead;
	@And("Click the Lead")
	public LeadPage clickLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleClickLead);
		return this;
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement eleCreateClickLead;
	@And("Click the CreateLead")
	public CreateLeadPage clickCreateLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateClickLead);
		return new CreateLeadPage();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleClickFindLead;
	public FindLead clickFindLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleClickFindLead);
		return new FindLead();
	}

		
		
		}
	






