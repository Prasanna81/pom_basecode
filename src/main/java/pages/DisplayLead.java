package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DisplayLead extends ProjectMethods {

	public DisplayLead() {
		PageFactory.initElements(driver, this);
	}
	
		@FindBy(linkText="Convert Lead")
		private WebElement eleConvertLead;
		public DisplayLead clickConvertLead() {
			
			//WebElemeneleClickCreateLeadagaint eleLogout = locateElement("class", "decorativeSubmit");
			click(eleConvertLead);
			return this;
		}
	}
	





