package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewPage extends ProjectMethods {

	public ViewPage() {
		PageFactory.initElements(driver, this);
	}
	
		@FindBy(linkText="Qualify Lead")
		private WebElement eleClickQualifyLead;
		public convertLead clickQualify() {
			
			//WebElemeneleClickCreateLeadagaint eleLogout = locateElement("class", "decorativeSubmit");
			click(eleClickQualifyLead);
			return new convertLead();
		}
	}
	





