package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLead extends ProjectMethods {

	public FindLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="10679")
	private WebElement eleenterLead;
	public FindLead enterLead() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleenterLead);
		return this;
	}
	@FindBy(xpath="//span[text()='Name and ID']")
	private WebElement eleclickName;
	public FindLead clickName() {
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleclickName);
		return this;
	}

	@FindBy(xpath="//input[@class=' x-form-text x-form-field ']")
	private WebElement eleentercompname;
	public FindLead entercompname()
	{
		type(eleentercompname, "Wipro");
		return this;
		
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleclickfind;
	public FindLead clickFind()
	{
		click(eleclickfind);
		return this;
		
	}
	
	//button[text()='Find Leads']
	

		
		
		}
	






