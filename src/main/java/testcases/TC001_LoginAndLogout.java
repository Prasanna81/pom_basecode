package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC001_LoginAndLogout";
		testDescription="Login into leaftaps";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String fname, String lname, String Cname, String loname ) {
		new LoginPage()
		.typeUsername("DemoSalesManager")
		.typePassword("crmsfa")
		.clickLogin()
		.clickCrmFsa().clickLead().clickCreateLead().enterName(fname).enterlastName(lname)
		.enterCompany(Cname).eleLocalName(loname).marketingCampaig(2).industry(3).currency(2).clickCreateLead().clickQualify().clickConvertLead();
		
	}
}
