package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC002_EditLead";
		testDescription="Edit the Leads";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void Findlead(String UName, String pwd ) {
		new LoginPage()
		.typeUsername(UName)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmFsa().clickLead().clickFindLead().clickName().entercompname().clickFind();
}
}
